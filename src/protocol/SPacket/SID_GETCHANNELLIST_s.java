package protocol.SPacket;

import protocol.ID;
import protocol.types.DWORD;

public class SID_GETCHANNELLIST_s extends _sPacket {

	DWORD sProduct_ID;

	public SID_GETCHANNELLIST_s(byte versionByte) {
		super(ID.SID_GETCHANNELLIST);
		switch (versionByte) {
		case ID.VersionByte_Warcraft_II:
			sProduct_ID = ID.ProductID_Warcraft_II;
			break;
		case ID.VersionByte_Warcraft_III:
		default:
			sProduct_ID = ID.ProductID_Warcraft_III;
			break;
		}

	}

	@Override
	protected void prepareData() {
		setData(sProduct_ID.getByteArray());
	}
}
